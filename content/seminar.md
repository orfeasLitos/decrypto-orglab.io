+++
template = "seminar.html"
aliases = ["seminars/"]

[[extra.seminars]]
author = "Dionysis Zindros"
affiliation = "University of Athens"
date = "2020-06-03"
time = "10:00:00"
timezone = "+03:00"
title = "Introduction to NIPoPoWs"
abstract = """
In this seminar, we will give an overview of Non-Interactive Proofs of Proof-of-Work and describe
how we can leverage superblocks to build them. We will also discuss the interlinking data structure
necessary to upgrade a blockchain for superblock support.
"""

[[extra.seminars]]
author = "Orfeas Stefanos Thyfronitis Litos"
affiliation = "University of Edinburgh"
date = "2020-06-10"
time = "10:00:00"
timezone = "+01:00"
title = "What is the Lightning Network?"
abstract = """
The high latency and low throughput of Bitcoin constitute two fundamental barriers for its wider adoption. The Lightning Network, an overlay protocol that can be run on top of Bitcoin, provides the most comprehensive solution to those issues by enabling unlimited off-chain payments with minimal latency between parties that share a payment channel.
In this talk we give an intuitive exposition of the Lightning protocol and its various capabilities. We describe the steps needed for setting up, using, and closing a 2-party Lightning channel, arguing that they are secure. We discuss the single disadvantage of Lightning compared to using the blockchain directly, namely that parties have to actively sync with the blockchain often to avoid losing funds. Finally, we highlight one of the most important -- and complex -- features of Lightning, multi-hop payments: leveraging well-understood cryptographic primitives and finely-tuned timeouts, a buyer can pay a seller even if they don't have an open channel between them.
"""
slides = "https://github.com/OrfeasLitos/PaymentChannels/raw/6b41c60375b0f51df93328942bc88f0a844cbb1a/ln-slides/slides.pdf"

[[extra.seminars]]
author = "Stelios Daveas"
affiliation = "University of Athens"
date = "2020-06-17"
time = "12:00:00"
timezone = "+03:00"
title = "Implementing a practical superlight Bitcoin client"
abstract = """
During the last years, significant effort has been put into enabling blockchain interoperability. Towards this goal, a new generation of verifiers have emerged called superlight clients. We focus on Non-Interactive Proofs of Proof of Work (NIPoPoW) superblock protocol, and we discuss a gas-efficient implementation for the verification of NIPoPoWs in Solidity. In particular, we explore patterns and techniques that considerably reduce gas consumption, and may also have applications to other smart contracts. We introduce a pattern that we term "hash-and-resubmit" that eliminates persistent storage almost entirely, leading to significant increase of performance. Furthermore, we alleviate the burden of expensive on-chain operations, which we transfer off-chain, and we make use of an optimistic schema that replaces functionalities of linear complexity with constant operations. Lastly, we make a cryptoeconomic analysis, and set concrete values regarding the cost of usage of our client.

Github: https://github.com/sdaveas/nipopow-verifier
"""
slides = "https://slides.com/steliosntaveas/nipopow-verifier/"
video = "https://www.youtube.com/watch?v=y0eahPHAgRc"

[[extra.seminars]]
author = "Zeta Avarikioti"
affiliation = "ETH Zürich"
date = "2020-06-24"
time = "12:00:00"
timezone = "+02:00"
title = "Brick: Asynchronous Payment Channels"
abstract = """
Off-chain protocols (channels) are a promising solution to the scalability and privacy challenges of blockchain payments. Current proposals, however, require synchrony assumptions to preserve the safety of a channel, leaking to an adversary the exact amount of time needed to control the network for a successful attack.
In this talk, we discuss BRICK, the first payment channel that remains secure under network asynchrony and concurrently provides correct incentives.
The core idea is to incorporate the conflict resolution process within the channel by introducing a rational committee of external parties, called wardens. Hence, if a party wants to close a channel unilaterally, it can only get the committee's approval for the last valid state.
BRICK provides sub-second latency because it does not employ heavy-weight consensus. Instead, BRICK uses consistent broadcast to announce updates and close the channel, a light-weight abstraction that is powerful enough to preserve safety and liveness to any rational parties.
"""
slides = "/slides/brick.pdf"

[[extra.seminars]]
author = "Christos Nasikas"
affiliation = "University of Athens"
date = "2020-07-01"
time = "12:00:00"
timezone = "+03:00"
title = "The Smart Contract Development Ecosystem in Ethereum"
abstract = """
In this presentation, we will give an overview of how we can develop Ethereum Dapps using the
tools availbable in the Ethereum ecosystem. We will discuss Truffle, a Javascript library and
framework used to develop and test smart contracts in Solidity; Ganache and Ganache-cli, a
local blockchain system which can be used to deploy smart contracts locally for developmpent purposes;
Metamask, a browser-based wallet which is particularly user-friendly when used with DApps;
and, finally, Remix, a web-based editor for your Solidity code which includes powerful
syntax checking and debugging capabilities. During the talk, we will include live demonstrations
and show you how these tools can be used together to develop complete and robust DApps.
"""

[[extra.seminars]]
author = "Kostis Karantias"
affiliation = "IOHK"
date = "2020-07-08"
time = "12:00:00"
timezone = "+03:00"
title = "A Taxonomy of Cryptocurrency Wallets"
abstract = """
The primary function of a cryptocurrency is money transfer between individuals. The wallet is the software that facilitates such transfers. Wallets are nowadays ubiquitous in the cryptocurrency space and a cryptocurrency is usually supported by many wallets. Despite that, the functionality of wallets has never been formally defined. Additionally, the mechanisms employed by the many wallets in the wild remain hidden in their respective codebases.

In this work we provide the first definition of a cryptocurrency wallet, which we model as a client to a server, or set of servers. We provide a distinction of wallets in various categories, based on whether they work for transparent or private cryptocurrencies, what trust assumptions they require, their performance and their communication overhead. For each type of wallet we provide a description of its client and server protocols. Additionally, we explore the superlight wallets and describe their difference to superlight clients that have appeared in recent literature. We demonstrate how new wallet protocols can be produced by combining concepts from existing protocols. Finally we evaluate the performance and security characteristics of all wallet protocols and compare them.
"""
video = "https://www.youtube.com/watch?v=LztcwUMKpTY"
slides = "/slides/a-taxonomy-of-cryptocurrency-wallets.pdf"

[[extra.seminars]]
author = "Orfeas Stefanos Thyfronitis Litos"
affiliation = "University of Edinburgh"
date = "2020-07-15"
time = "10:00:00"
timezone = "+01:00"
title = "From Channels to Network: Off-chain Multi-hop Payments in Lightning"
abstract = "Building upon the ideas of pairwise Lightning channels, we explore Hashed TimeLocked Contracts (HTLCs), the technique that allows atomic payments along paths over channels. This technique expands the individual channels into an interconnected network of nodes that can freely exchange funds off-chain with any reachable node with minimal latency. Nodes can now transact even without having a direct channel, thus the need for a complete graph between nodes is relaxed -- just a connected graph with sufficient capacity is enough for any party to transact with any other. We will understand why parties along a payment path can trustlessly and securely send, receive, or facilitate the transfer of value. We will also discuss a number of tricks and optimizations in LN that turn the theoretical ideas of payment channels into a practical, incentivized, deployable and robust layer-2 payment network."
slides = "https://github.com/OrfeasLitos/PaymentChannels/raw/99c4b21303dd403d40054e36596a93b9cc1f1001/ln-slides/multi-hop-slides.pdf"
video = "https://vimeo.com/439191722"

[[extra.seminars]]
author = "Andrianna Polydouri"
affiliation = "University of Athens"
date = "2020-07-22"
time = "12:00:00"
timezone = "+03:00"
title = "A Study on Superlight Blockchain Clients under Velvet Fork"
abstract = """
In this presentation, we investigate how a blockchain can be upgraded to support superblock clients without a soft fork. We show that it is possible to implement the needed changes without modifying the consensus protocol and by requiring only a minority of miners to upgrade, a process termed a velvet fork in the literature. While previous work conjectured that Superblock and FlyClient clients can be safely deployed using velvet forks as-is, we show that previous constructions are insecure. We describe a novel class of attacks, called chain-sewing, which arise in the velvet fork setting: an adversary can cut-and-paste portions of various chains from independent forks, sewing them together to fool a superlight client into accepting a false claim. We show how previous velvet fork constructions can be attacked via chain-sewing. Next we put forth the first provably secure velvet superblock client construction which we show  secure against adversaries that are bounded by 1/3 of the upgraded honest miner population.
"""
slides = "https://slides.com/andrian/nipopows-under-velvet-fork-b141ab"
video = "https://vimeo.com/455510847"

[[extra.seminars]]
author = "Alex Chepurnoy"
affiliation = "Ergo Platform"
date = "2020-09-02"
time = "12:00:00"
timezone = "+03:00"
title = "Bypassing Non-Outsourceable Proof-of-Work Schemes Using Collateralized Smart Contracts"
abstract = """
Centralized pools and renting of mining power are considered as sources of possible censorship threats and even 51% attacks for decentralized cryptocurrencies. Non-outsourceable Proof-of-Work schemes have been proposed to tackle these issues. However, tenets in the folklore say that such schemes could potentially be bypassed by using escrow mechanisms.

We consider how to bypass non-outsourceable Proof-of-Work schemes using collateralized smart contracts. Our approach allows for that if the underlying blockchain platform supports smart contracts in a sufficiently advanced language. In particular, the language should allow access to the PoW solution.

At a high level, our approach requires the miner to lock collateral covering the reward amount and protected by a smart contract that acts as an escrow. The smart contract has logic that allows the pool to collect the collateral as soon as the miner collects any block reward. We propose two variants of the approach depending on when the collateral is bound to the block solution. Using this, we show how to bypass previously proposed non-outsourceable Proof-of-Work schemes (with the notable exception for strong non-outsourceable schemes) and show how to build mining pools for such schemes.
"""
video = "https://www.youtube.com/watch?v=4_sU_BDNAMs"

[[extra.seminars]]
author = "Dimitris Karakostas"
affiliation = "University of Edinburgh"
date = "2020-09-09"
time = "12:00:00"
timezone = "+03:00"
title = "Efficient State Management in Distributed Ledgers"
abstract = """
Distributed ledgers implement a storage layer, on top of which a shared state which is maintained by all participants in a decentralized manner. In UTxO-based ledgers, like Bitcoin, the shared state is the set of all unspent outputs (UTxOs), which serve as inputs to future transactions. However, decentralized systems lack centrally planned policies to enforce proper maintenance of the shared state, such as garbage collection, which would enhance performance and prevent Denial-of-Service (DoS) attacks. Instead, the system's design aims at incentivizing participants to act as intended. In this talk we investigate correct practices, such that the shared state in distributed ledgers is managed efficiently. We consider an abstract ledger model and investigate a set of transaction optimization techniques, including a coin selection algorithm which aims at minimizing the shared state. We also define state efficiency, which is the necessary property that a distributed ledger's fee scheme should possess in order to incentivize efficient state management, and propose an amended, efficient fee function for Bitcoin.
"""

[[extra.seminars]]
author = "George Kadianakis"
affiliation = "University of Athens, Tor Project"
date = "2020-09-16"
time = "10:00:00"
timezone = "+01:00"
title = "Proving Work Over Onions: PoW applications in Tor"
abstract = """
"""

[[extra.seminars]]
author = "Thomas Kerber"
affiliation = "University of Edinburgh"
date = "2020-09-23"
time = "10:00:00"
timezone = "+01:00"
title = "Privacy & Smart Contracts"
abstract = """
Smart contracts have drastically lowered the barrier to entry for
designing and deploying distributed systems. Largely they have done so
at a great cost to privacy however, which smart contracts often ignore
completely. In this talk we will see various approaches to
re-introducing privacy in this setting, and what difficulties they
encounter.
"""
video="https://vimeo.com/461002091"
slides="https://drwx.org/2020/09/23/decrypto-slides.pdf"

[[extra.seminars]]
author = "Amitabh Saxena"
affiliation = "Ergo Platform"
date = "2020-09-30"
time = "16:00:00"
timezone = "+03:00"
title = "ZeroJoin: Combining Zerocoin and CoinJoin"
abstract = """
We present ZeroJoin, a practical privacy-enhancing protocol for blockchain transactions. ZeroJoin can be considered a combination of Zerocoin and CoinJoin. Like Zerocoin, our protocol uses zero-knowledge proofs and a pool of participants. However, unlike Zerocoin, our proofs are very efficient, and our pool size is not monotonically increasing. Thus, our protocol overcomes the two major drawbacks of Zerocoin. Our approach can also be considered a non-interactive variant of CoinJoin, where the interaction is replaced by a public transaction on the blockchain. We also present ErgoMix, a practical implementation of ZeroJoin on top of Ergo, a smart contract platform based on Sigma protocols. While ZeroJoin contains the key ideas, it leaves open the practical issue of handling fees. The key contribution of ErgoMix is a novel approach to handle fee in ZeroJoin.
"""
video = "https://www.youtube.com/playlist?list=PL8-KVrs6vXLSKEm64jBuzxYllZxGsacQw"

[[extra.seminars]]
author = "Lefteris Kokoris-Kogias"
affiliation = "IST Austria"
date = "2020-10-07"
time = "11:00:00"
timezone = "+02:00"
title = "Digital Trust and Decentralization"
abstract = """
In the last decades, computing managed to scale societies and bring everyone closer. We live in an era of efficiency and speed.
Speed, however, is the enemy of trust. Trust needs friction and time to be built.

In this talk, we are going to explore the evolution of trust and investigate the latest explosion of interest in digital and decentralized trust technologies.
To this end, I am going to present both theoretical and practical advancements of my research in the last few years focusing on Byzantine Fault Tolerant systems and algorithms, answering questions such as: how can we get scalable decentralized systems able to support the current financial ecosystems, and how can we scavenge randomness from multiple semi-trustworthy players to run publicly-verifiable lotteries or to audit elections?
"""
authorlink="https://ist.ac.at/en/research/kokoris-group/"
video="https://www.youtube.com/watch?v=ftrXpSmwR0I"
slides="/slides/digital-trust-and-decentralization.pdf"

[[extra.seminars]]
author = "Pyrros Chaidos"
affiliation = "University of Athens"
date = "2020-10-14"
time = "12:00:00"
timezone = "+03:00"
title = "Aggregate weighted signatures for Blockchain Bootstrapping"
abstract = """
Proof of Stake Blockchains operate by having participants demonstrate that
they control a certain piece of stake, chosen by some selection mechanism.
In contrast, proof of work blockchains require that participants provide a
solution to some computational problem with restrictions indicated by the context.
Such solutions can usually be verified in isolation and at low computational cost.

In many current Proof of Stake implementations, verifying a claim of stake either
requires a large context or a somewhat complex proof. These costs are exacerbated
when a new user wishes to participate for the first time, as verifying the current
state will often require replaying operations since the genesis.

We introduce the primitive of weighted aggregate signatures and use it to offer a
simple solution to speed up bootstrapping: we produce an aggregate signature
for verifying a succinct checkpoint for each epoch. Our approach minimizes computational
costs to small stakeholders and is compatible with recent advances such as key-evolving
signatures and delegation.
"""
video="https://www.youtube.com/watch?v=Rnw_CtmI1oc"
slides="/slides/aggregate-weighted-signatures-for-blockchain-bootstrapping.pptx"

[[extra.seminars]]
author = "Michele Ciampi"
affiliation = "University of Edinburgh"
date = "2020-10-21"
time = "12:00:00"
timezone = "+03:00"
title = "Updatable Blockchains"
abstract = """
Software updates for blockchain systems become a real challenge when they impact the underlying consensus mechanism. The activation of such changes might jeopardize the integrity of the blockchain by resulting in chain splits. Moreover, the software update process should be handed over to the community and this means that the blockchain should support updates without relying on a trusted party. In this paper, we introduce the notion of updatable blockchains and show how to construct blockchains that satisfy this definition. Informally, an updatable blockchain is a secure blockchain and in addition it allows to update its protocol preserving the history of the chain. In this work, we focus only on the processes that allow securely switching from one blockchain protocol to another assuming that the blockchain protocols are correct. That is, we do not aim at providing a mechanism that allows reaching consensus on what is the code of the new blockchain protocol. We just assume that such a mechanism exists (like the one proposed in NDSS 2019 by Zhang et. al), and show how to securely go from the old protocol to the new one. The contribution of this paper can be summarized as follows. We provide the first formal definition of updatable ledgers and propose the description of two compilers. These compilers take a blockchain and turn it into an updatable blockchain. The first compiler requires the structure of the current and the updated blockchain to be very similar (only the structure of the blocks can be different) but it allows for an update process more simple, efficient. The second compiler that we propose is very generic (i.e., makes few assumptions on the similarities between the structure of the current blockchain and the update blockchain). The drawback of this compiler is that it requires the new blockchain to be resilient against a specific adversarial behaviour and requires all the honest parties to be online during the update process. However, we show how to get rid of the latest requirement (the honest parties being online during the update) in the case of proof-of-work and proof-of-stake ledgers.
"""
video="https://www.youtube.com/watch?v=aYh7PsLlMNo"
slides="/slides/updatable-blockchains.pdf"

[[extra.seminars]]
author = "Dragos Ilie"
affiliation = "Imperial College London"
date = "2020-11-04"
time = "12:00:00"
timezone = "+02:00"
title = "Committing to Quantum Resistance: Defences for Bitcoin Quantum Adversaries"
abstract = """
Quantum computers are expected to have a dramatic impact on numerous fields, due to their anticipated ability to solve classes of mathematical problems much more efficiently than their classical counterparts. This particularly applies to domains involving integer factorisation and discrete logarithms, such as public key cryptography.

In this talk, we consider the threats a quantum-capable adversary could pose to Bitcoin, which currently uses the Elliptic Curve Digital Signature Algorithm (ECDSA) to sign transactions.

We then propose a simple commit--delay--reveal protocol and a variant of it where the security parameter is configurable. These schemes allow users to securely move their funds from old (non-quantum-resistant) outputs to those adhering to a quantum-resistant digital signature scheme.
"""
video="https://www.youtube.com/watch?v=iYUAESirLB8"

[[extra.seminars]]
author = "Giorgos Panagiotakos"
affiliation = "University of Athens"
date = "2020-11-11"
time = "12:00:00"
timezone = "+02:00"
title = "Blockchains from Non-Idealized Hash Functions"
abstract = """
The formalization of concrete, non-idealized hash function properties sufficient
to prove the security of Bitcoin and related protocols has been elusive, as all
previous security analyses of blockchain protocols have been performed in the
random oracle model. In this paper we identify three such properties, and then
construct a blockchain protocol whose security can be reduced to them in the
standard model assuming a common reference string (CRS).The three properties
are: collision resistance, computational randomness extraction and iterated
hardness. While the first two properties have been extensively studied, iterated
hardness has been empirically stress-tested since the rise of Bitcoin; in fact,
as we demonstrate in this paper, any attack against it (assuming the other two
properties hold) results in an attack against Bitcoin.In addition, iterated
hardness puts forth a new class of search problems which we term iterated search
problems (ISP). ISPs enable the concise and modular specification of blockchain
protocols, and may be of independent interest.
"""
slides="/slides/blockchains-from-non-idealized-hash-functions.pdf"

[[extra.seminars]]
author = "Alexei Zamyatin"
affiliation = "Imperial College London and Interlay"
date = "2020-11-18"
time = "12:00:00"
timezone = "+02:00"
title = "Trustless Cross-Chain Communication: Impossible but Incentive Compatible"
abstract = """
Since the advent of Bitcoin, a plethora of distributed ledgers has been created, differing in design and purpose. Considering it is unlikely that there will be "one coin to rule them all", interoperability has shifted into the focus of industry and academia. Today, cross-chain communication (CCC) plays a fundamental role not only in cryptocurrency exchanges, but also in scalability efforts via sharding, extension of existing systems through sidechains, and bootstrapping of new blockchains.

In this talk, we formulate the problem of cross-chain communication (CCC) and show that CCC is impossible without a trusted third party, contrary to common beliefs in the blockchain community. We then present XCLAIM, a framework for migrating assets across blockchains which works around this result by leveraging incentives. XCLAIM employs a dynamic and permissionless set of collateralized intermediaries, combined with cross-chain state verification and punishment. This construction ensures that users do not face financial damage from theft or collusion of intermediaries, while maintaining transparency and usability.
"""
authorlink="https://www.alexeizamyatin.me/"
video="https://www.youtube.com/watch?v=2NgQ3cJZw_M"

[[extra.seminars]]
author = "Runchao Han"
affiliation = "Monash University"
date = "2020-11-25"
time = "12:00:00"
timezone = "+02:00"
title = "RandChain: Decentralised Randomness Beacon from Sequential Proof-of-Work"
abstract = """
Decentralised Randomness Beacon (DRB) is a service that generates publicly verifiable randomness. Constructing DRB protocols is challenging. Existing DRB protocols suffer from strong network synchrony assumptions, high communication complexity and/or various attacks. In this paper, we propose RandChain, a new family of DRB protocols. RandChain is constructed from Sequential Proof-of-Work (SeqPoW), a Proof-of-Work (PoW) variant that is sequential, i.e., the work can only be done by a single processor. In RandChain, nodes jointly maintain a blockchain, and each block derives a random output. To append a block to the blockchain, each node should keep mining, i.e., solve a SeqPoW puzzle derived from the last block and its identity. Given the SeqPoW and its fixed input, mining is non-parallelisable. RandChain applies Nakamoto consensus so that nodes agree on a unique blockchain.

While inheriting simplicity and scalability from Nakamoto consensus, RandChain produces strongly unpredictable randomness and remains energy-efficient and decentralised. RandChain does not require nodes to provide local entropy, thus giving no opportunity to bias randomness. Solutions of SeqPoW puzzles are unpredictable, so nodes cannot predict randomness. As each node can use at most a single processor for mining, RandChain remains energy-efficient. The mining speed is bounded by processors’ clock rate, which is hard to improve further. Thus, powerful nodes can gain limited advantage over mining, and RandChain achieves a high degree of decentralisation.
"""
slides="https://docs.google.com/presentation/d/11Gx7NhR2znCe8sLlBka7ITphm_M8eL9ErCEbjuBGNCw/edit?usp=sharing"
video="https://www.youtube.com/watch?v=l4XSbWaKDVY"

[[extra.seminars]]
author = "Mustafa Al-Bassam"
affiliation = "University College London"
date = "2020-12-02"
time = "12:00:00"
timezone = "+02:00"
title = "LazyLedger: A Distributed Data Availability Ledger With Client-Side Smart Contracts"
abstract = """
We propose LazyLedger, a design for distributed ledgers where the blockchain is optimised for solely ordering and guaranteeing the availability of transaction data. Responsibility for executing and validating transactions is shifted to only the clients that have an interest in specific transactions relating to blockchain applications that they use. As the core function of the consensus system of a distributed ledger is to order transactions and ensure their availability, consensus participants do not necessarily need to be concerned with the contents of those transactions. This reduces the problem of block verification to data availability verification, which can be achieved probabilistically with sub-linear complexity, without downloading the whole block. The amount of resources required to reach consensus can thus be minimised, as transaction validity rules can be decoupled from consensus rules. We also implement and evaluate several example LazyLedger applications, and validate that the workload of clients of specific applications does not significantly increase when the workload of other applications that use the same chain increase.
"""
slides="/slides/lazyledger.pdf"

[[extra.seminars]]
author = "Erkan Tairi"
affiliation = "TU Wien"
date = "2020-12-09"
time = "12:00:00"
timezone = "+02:00"
title = "Post-Quantum Adaptor Signatures for Privacy-Preserving Off-Chain Payments"
abstract = """
Adaptor signatures (AS) are an extension of digital signatures that enable the encoding of a cryptographic hard problem (e.g., discrete logarithm) within the signature itself. An AS scheme ensures that (i) the signature can be created only by the user knowing the solution to the cryptographic problem; (ii) the signature reveals the solution itself; (iii) the signature can be verified with the standard verification algorithm. These properties have made AS a salient building block for many blockchain applications, in particular, off-chain payment systems such as payment-channel networks, payment-channel hubs, atomic swaps or discrete log contracts. Current AS constructions, however, are not secure against adversaries with access to a quantum computer. In this talk, we present constructions for adaptor signatures that rely on cryptographic assumptions that are post-quantum secure, and show how they can be seamlessly leveraged to build post-quantum off-chain payment applications such as payment-channel networks without harming their security and privacy.
"""
slides = "/slides/post-quantum-adaptor-signatures.pdf"

[[extra.seminars]]
author = "Maxim Jourenko"
affiliation = "Tokyo Institute of Technology"
date = "2021-01-20"
time = "18:00:00"
timezone = "+09:00"
title = "Payment Trees: Low Collateral Payments for Payment Channel Networks"
abstract = """
The security of blockchain based decentralized ledgers relies on consensus protocols executed between mutually distrustful parties. Such protocols incur delays which severely limit the throughput of such ledgers. Payment and state channels enable execution of offchain protocols that allow interaction between parties without involving the consensus protocol. Protocols such as Hashed Timelock Contracts (HTLC) and Sprites (FC'19) connect channels into Payment Channel Networks (PCN) allowing payments across a path of payment channels. Such a payment requires each party to lock away funds for an amount of time. The product of funds and locktime is the collateral of the party, i.e., their cost of opportunity to forward a payment. In the case of HTLC, the locktime is linear to the length of the path, making the total collateral invested across the path quadratic in size of its length. Sprites improved on this by reducing the locktime to a constant by utilizing smart contracts. We propose the Payment Trees protocol that allows payments across a PCN with linear total collateral without the aid of smart contracts. A competitive performance similar to Sprites, and yet compatible to Bitcoin.
"""
link="https://zoom.us/j/97866745516?pwd=SHU5bUhyb3BITk9ibWplTTBya2I2dz09"
password="8JU5mM"

[[extra.seminars]]
author = "Charalampos Papamanthou"
affiliation = "University of Maryland"
date = "2021-01-27"
time = "18:00:00"
timezone = "+02:00"
title = "Distributed Authenticated Data Structures and their Applications"
abstract = """
We put forth a new model for distributed authenticated data structures. In a distributed authenticated data structure of n elements and p parties, every party stores n/p elements along with their corresponding proofs; a designated trusted party holds the digest of the authenticated data structure. Whenever an update is issued to the data structure, all parties should be able to efficiently update their local proofs and digests. Additionally, a party that has received multiple (verifying) proofs from other parties should be able to produce a small aggregate proof and forward it to another party for verification. Traditional Merkle trees and other vector commitments are not distributed since they fail to support efficient distributed updates or to achieve aggregation. After a short literature review, we will present a new distributed authenticated data structure, called multilinear tree, that satisfies our objectives. We will conclude with some open problems in the area, along with applications of distributed authenticated data structures in stateless validation for cryptocurrencies.
"""
link="https://zoom.us/j/98490153604?pwd=RHpOUm4xS1BrU1ovaWN2WDFBWExIZz09"
password="pUaR3c"
slides="/slides/distributed-authenticated-data-structures.pptx"

[[extra.seminars]]
author = "Giorgos Vlachos"
affiliation = "Axelar"
date = "2021-02-03"
time = "18:00:00"
timezone = "+02:00"
title = "Leader Based Consensus in Proof of Stake Networks"
abstract = """
Byzantine fault tolerant state machine replication protocols are core building blocks for Proof of Stake (PoS) networks. The starting point for many such systems is Practical Byzantine Fault Tolerance (PBFT). Unfortunately, PBFT is not suitable for the permissionless setting, because it works best when the leader that proposes blocks of transactions is fixed for a long time period and always behaves honestly. For PoS networks, anybody should be able to become a leader, and thus this assumption is easily violated. Several recent protocols such as Casper, Algorand, Tendermint, and HotStuff, allow for efficient leader rotation and are thus more suitable for the permissionless setting. In this talk, we will review some of these protocols and compare their models and efficiency.
"""
link="https://zoom.us/j/91310806434?pwd=Y2E5MHhTakwwb05YcG5nQThWRWJqZz09"
password="Zua0jr"

[[extra.seminars]]
author = "Romiti Matteo and Friedhelm Victor"
affiliation = "Austrian Institute of Technology and Technische Universität Berlin"
date = "2021-02-10"
time = "18:00:00"
timezone = "+02:00"
title = "Cross-Layer Deanonymization Methods in the Lightning Protocol"
abstract = """
Bitcoin (BTC) pseudonyms (layer 1) can effectively be deanonymized using heuristic clustering techniques. However, while performing transactions off-chain (layer 2) in the Lightning Network (LN) seems to enhance privacy, a systematic analysis of the anonymity and privacy leakages due to the interaction between the two layers is missing. We present clustering heuristics that group BTC addresses, based on their interaction with the LN, as well as LN nodes, based on shared naming and hosting information. We also present linking heuristics that link 45.97% of all LN nodes to 29.61% BTC addresses interacting with the LN. These links allow us to attribute information (e.g., aliases, IP addresses) to 21.19% of the BTC addresses contributing to their deanonymization. Further, these deanonymization results suggest that the security and privacy of LN payments are weaker than commonly believed, with LN users being at the mercy of as few as five actors that control 36 nodes and over 33% of the total capacity. Overall, this is the first paper to present a method for linking LN nodes with BTC addresses across layers and to discuss privacy and security implications.
"""
link="https://zoom.us/j/98734915337?pwd=UmNNQmNyVmUvT2c3dHZ6UjgrUnhhZz09"
password="D9ES29"

[[extra.seminars]]
author = "Andrew Miller"
affiliation = "University of Illinois Urbana-Champaign"
date = "2021-02-17"
time = "18:00:00"
timezone = "+02:00"
title = "Using Universal Composability to implement off-chain payment channels"
abstract = """
In this talk we’ll discuss work in progress on implementing payment channels in Python-SaUCy, a library for distributed protocols based on Universal Composability (UC). UC is a formal framework originating in cryptography and used for the on-paper analysis of protocols including blockchains and off-chain payment channels, but it isn’t something that software developers use.

Our experience building a simple payment channel in Python-SaUCy shows that there are already several benefits. The software comes with an executable specification, in the form of a UC ideal functionality. You can execute our payment channel specification to better understand what the protocol does. We also support test driven fault tolerance analysis directly based on the security specification. It can represent test cases where the environment produces different output in the ideal world than in the real world. This gives some view of how in the future, formal security specifications could be packaged along with full-featured payment channels and other blockchain software releases.
"""
link="https://zoom.us/j/94409791303?pwd=aWp5RHFYZVVlRWV1Tkw5N1RmdXNrdz09"
password="0C9mjs"

[[extra.seminars]]
author = "Lukas Aumayr"
affiliation = "TU Wien"
date = "2021-02-24"
time = "12:00:00"
timezone = "+02:00"
title = "Blitz: Secure Multi-Hop Payments Without Two-Phase-Commits"
abstract = """
Payment-channel networks (PCN) are the most prominent approach to tackle the scalability issues of current permissionless blockchains. A PCN reduces the load on-chain by allowing arbitrarily many off-chain multi-hop payments (MHP) between any two users connected through a path of payment channels. Unfortunately, current protocols for MHP are far from satisfactory. One round MHP (e.g., Interledger) are insecure as a malicious intermediary can steal the payment funds. Two-round MHP (e.g., Lightning Network (LN)) follow the 2-phase-commit paradigm as in databases to overcome this issue. However, when tied with economical incentives, 2-phase-commit brings other security threats (i.e., wormhole attacks), staggered collateral (i.e., funds are locked for a time proportional to the payment path length) and dependency on specific scripting language functionality (e.g., hash time lock contracts) that hinders a wider deployment in practice.

We present Blitz, a novel MHP protocol that demonstrates for the first time that we can achieve the best of the two worlds: a single round MHP where no malicious intermediary can steal coins. Moreover, Blitz provides privacy for sender and receiver, it is not prone to the wormhole attack and it requires only constant collateral. Additionally, we construct MHP using only digital signatures and a timelock functionality, both available at the core of virtually every cryptocurrency today. We provide the cryptographic details of Blitz and we formally prove its security. Furthermore, our experimental evaluation on an LN snapshot shows that the LN collateral results in between 4x and 33x more unsuccessful payments than the Blitz collateral (both compared to a baseline). Blitz reduces the size of the payment contract by 26% and prevents up to 0.3 BTC (3397 USD in October 2020) in fees being stolen over a three day period as it avoids wormhole attacks by design.
"""
link="https://zoom.us/j/97995626726?pwd=c1VXbks0dW9xRVpEc2d5RCtpaVQzQT09"
password="pjtQq3"
video="https://www.youtube.com/watch?v=pk0eNlO-hUY"
slides="/slides/blitz.pdf"

[[extra.seminars]]
date = "2021-03-03"
placeholder = true
announcement = """
On March 3rd, the seminar will not take place. We invite you to attend the <a href='https://ifca.ai/fc21/'>Financial Crypto 2021</a> conference
taking place online this week.
"""

[[extra.seminars]]
author = "Chrysoula Stathakopoulou"
affiliation = "IBM Research"
date = "2021-03-10"
time = "18:00:00"
timezone = "+02:00"
title = "Mir-BFT: High-Throughput Robust BFT for Decentralized Networks"
abstract = """
This paper presents Mir-BFT, a robust Byzantine fault-tolerant (BFT) total order broadcast protocol aimed at maximizing throughput on wide-area networks (WANs), targeting deployments in decentralized networks, such as permissioned and Proof-of-Stake permissionless blockchain systems.
Mir-BFT is the first BFT protocol that allows multiple leaders to propose request batches independently (i.e., parallel leaders), in a way that precludes request duplication attacks by malicious (Byzantine) clients, by rotating the assignment of a partitioned request hash space to leaders.
As this mechanism removes a single-leader bandwidth bottleneck and exposes a computation bottleneck related to authenticating clients even on a WAN, our protocol further boosts throughput using a client signature verification sharding optimization.
Our evaluation shows that Mir-BFT outperforms state-of-the-art and orders more than 60000 signed Bitcoin-sized (500-byte) transactions per second on a widely distributed 100 nodes, 1 Gbps WAN setup, with typical latencies of few seconds.
We also evaluate Mir-BFT under different crash and Byzantine faults, demonstrating its performance robustness.
Mir-BFT relies on classical BFT protocol constructs, which simplifies reasoning about its correctness.
Specifically, Mir-BFT is a generalization of the celebrated and scrutinized PBFT protocol. In a nutshell, Mir-BFT follows PBFT "safety-wise", with changes needed to accommodate novel features restricted to PBFT liveness.
"""
link="https://zoom.us/j/93296471885?pwd=NXJhS0ZHWUh5RC9uU2hLc0NvVFdzZz09"
password="n3vFMv"

[[extra.seminars]]
author = "Giorgos Tsimos"
affiliation = "University of Maryland"
date = "2021-03-17"
time = "18:00:00"
timezone = "+02:00"
title = "Gossiping For Communication-Efficient Broadcast"
abstract = """
Broadcast (BC) is a crucial ingredient for a plethora of cryptographic protocols such as secret sharing and multiparty computation.
In this talk, we present new randomized BC protocols with improved communication complexity and secure against an adversary controlling the majority of parties.
We utilize gossiping i.e. propagating a message by sending to a few random parties who in turn do the same, until the message is delivered.
We show a protocol for single-sender BC that achieves O (n^2 κ^2) bits of communication and where no trusted setup is required.
We also present, to the best of our knowledge, the first non-trivial, adaptively-secure parallel BC protocol (widely used in MPC) with O (n^2 κ^4) communication complexity, improving existing parallel BC protocols of O(n^3) communication.
"""
link="https://zoom.us/j/94658788236?pwd=bURlcFU0aEJhSDR3cUI5cXkrbGlOdz09"
password="6y9Edn"

[[extra.seminars]]
author = "Dimitris Karakostas"
affiliation = "University of Edinburgh"
date = "2021-03-24"
time = "18:00:00"
timezone = "+02:00"
title = "A Brief History of Currency Stability"
abstract = """
What connects the 1600s Dutch Republic to the early 21st century digital space? What were the biggest economic and monetary debates since Adam Smith wrote the "Wealth of Nations"? What do cryptocurrencies stand to learn from the Great Crises of capitalism? Why is Bitcoin's price volatile and how do stablecoins attempt to solve this problem? In our presentation, we try to answer these questions and understand how Bitcoin and stablecoins fit in the brief history of economics.
"""
link="https://zoom.us/j/94353719610?pwd=NzIyclgrdkFrcjZ1em8rS3NrMlQvQT09"
password="dG3Cu7"
video="https://www.youtube.com/watch?v=87fdlsyCnOY"
slides="/slides/stablecoins.pdf"

[[extra.seminars]]
author = "Lioba Heimbach"
affiliation = "ETH Zurich"
date = "2021-03-31"
time = "18:00:00"
timezone = "+03:00"
title = "FnF-BFT: Exploring Performance Limits of BFT Protocols"
abstract = """
We introduce FnF-BFT, a parallel-leader byzantine fault-tolerant state-machine replication protocol for the partially synchronous model
with theoretical performance bounds during synchrony. By allowing all replicas to act as leaders and propose requests independently,
FnF-BFT parallelizes the execution of requests. Leader parallelization distributes the load over the entire network – increasing
throughput by overcoming the single-leader bottleneck. We further use historical data to ensure that well-performing replicas are
in command. FnF-BFT’s communication complexity is linear in the number of replicas during synchrony and thus competitive
with state-of-the-art protocols. Finally, with FnF-BFT, we introduce the first BFT protocol with performance guarantees in stable
network conditions under truly byzantine attacks. A prototype implementation of FnF-BFT outperforms (state-of-the-art) HotStuff’s
throughput, especially as replicas increase, showcasing FnF-BFT’s significantly improved scaling capabilities.
"""
link="https://zoom.us/j/95967912472?pwd=MHNHNlppNnJXT2JXRHNjNFJuZEpWZz09"
password="GC7QRm"
slides="/slides/fnf-bft.pdf"

[[extra.seminars]]
author = "Joachim Neu"
affiliation = "Stanford University"
date = "2021-04-07"
time = "18:00:00"
timezone = "+03:00"
title = "Resolving the Availability-Finality Dilemma, or, How to Fix Ethereum 2.0's Consensus Protocol"
abstract = """
The CAP theorem says that no consensus protocol can be live under
dynamic participation and safe under network partitions. To resolve
this availability-finality dilemma, we formalize a new class of
flexible consensus protocols, ebb-and-flow protocols, which support a
full dynamically available ledger in conjunction with a finalized
prefix ledger. The finalized ledger falls behind the full ledger when
the network partitions but catches up when the network heals. Gasper,
the current candidate for Ethereum 2.0's beacon chain, aims to achieve
this property, but we discovered an attack in the standard synchronous
network model. We present a construction of provably secure ebb-and-
flow protocols with optimal resilience, based on black-box composition
of an off-the-shelf dynamically available and an off-the-shelf
partially synchronous BFT protocol.
"""
link="https://zoom.us/j/98832732443?pwd=ZXRYSDVOTStHc2UyOUozVERyRmJjdz09"
password="KhxH3h"

[[extra.seminars]]
author = "Ertem Nusret Tas"
affiliation = "Stanford University"
date = "2021-04-14"
time = "18:00:00"
timezone = "+03:00"
title = "Everything is a Race and Nakamoto Always Wins"
abstract = """
Nakamoto invented the longest chain protocol, and claimed its security by analyzing the private double-spend attack, a race between the adversary and the honest nodes to grow a longer chain. But is it the worst attack? We answer the question in the affirmative for three classes of longest chain protocols, designed for different consensus models: 1) Nakamoto's original Proof-of-Work protocol; 2) Ouroboros and SnowWhite Proof-of-Stake protocols; 3) Chia Proof-of-Space protocol. As a consequence, exact characterization of the maximum tolerable adversary power is obtained for each protocol as a function of the average block time normalized by the network delay. The security analysis of these protocols is performed in a unified manner by a novel method of reducing all attacks to a race between the adversary and the honest nodes.
"""
link="https://zoom.us/j/98572984704?pwd=ZGVCU1pOL0ZUZmlkaGNFdjd2N2xhdz09"
password="AES7LN"

[[extra.seminars]]
author = "Sandro Coretti-Drayton"
affiliation = "IOHK"
date = "2021-04-28"
time = "18:00:00"
timezone = "+02:00"
title = "(Title to be determined)"
abstract = """
"""
link="https://zoom.us/j/94233926850?pwd=UVlEeW11enNCczJJZVFEbjk0aHQrUT09"
password="drfpv2"

[[extra.seminars]]
author = "Ankush Das"
affiliation = "Carnegie Mellon University"
date = "2021-05-05"
time = "18:00:00"
timezone = "+03:00"
title = "Resource-Aware Session Types for Digital Contracts"
abstract = """
While there exist several successful techniques for supporting programmers in deriving static resource bounds for sequential code, analyzing the resource usage of message-passing concurrent processes poses additional challenges. To meet these challenges, this talk presents an analysis for statically deriving worst-case bounds on the computational complexity of message-passing processes. The analysis is based on novel resource-aware session types that describe resource contracts by allowing both messages and processes to carry potential and amortize cost. The talk then applies session types to implement digital contracts. Digital contracts are protocols that describe and enforce execution of a contract, often among mutually distrusting parties. Programming digital contracts comes with its unique challenges, which include describing and enforcing protocols of interaction, analyzing resource usage and tracking linear assets. The talk presents the type-theoretic foundations of Nomos, a programming language for digital contracts whose type system addresses the aforementioned domain-specific requirements. To express and enforce protocols, Nomos is based on shared binary session types rooted in linear logic. To control resource usage, Nomos uses resource-aware session types and automatic amortized resource analysis, a type-based technique for inferring resource bounds. To track assets, Nomos employs a linear type system that prevents assets from being duplicated or discarded. The talk concludes with future work directions on designing an efficient implementation for Nomos and making it robust to attacks from malicious entities.
"""
link="https://zoom.us/j/91520809738?pwd=cTFuUWlTME9sOVA4WUZoa3pRcUFiZz09"
password="qga6Yz"

[[extra.seminars]]
author = "Srivatsan Sridhar"
affiliation = "Stanford University"
date = "2021-05-12"
time = "18:00:00"
timezone = "+03:00"
title = "(To be determined)"
abstract = """
"""
link="https://zoom.us/j/92797219557?pwd=dnh6L21Sc0tkUzh0MjdmaEh4UWhEUT09"
password="UpLbf4"

[[extra.seminars]]
author = "Zeta Avarikioti"
affiliation = "ETH Zurich and IST Austria"
date = "2021-06-02"
time = "18:00:00"
timezone = "+03:00"
title = "Divide and Scale: Formalization of Distributed Ledger Sharding Protocols"
Sharding distributed ledgers is the most promising on-chain solution for scaling blockchain technology. In this work, we define and analyze the properties a sharded distributed ledger should fulfill. More specifically, we show that a sharded blockchain cannot be scalable under a fully adaptive adversary, but it can scale up to O(n/logn) under an epoch-adaptive adversary. This is possible only if the distributed ledger creates succinct proofs of the valid state updates at the end of each epoch. Our model builds upon and extends the Bitcoin backbone protocol by defining consistency and scalability. Consistency encompasses the need for atomic execution of cross-shard transactions to preserve safety, whereas scalability encapsulates the speedup a sharded system can gain in comparison to a non-sharded system. We introduce a protocol abstraction and highlight the sufficient components for secure and efficient sharding in our model. In order to show the power of our framework, we analyze the most prominent sharded blockchains (Elastico, Monoxide, OmniLedger, RapidChain) and pinpoint where they fail to meet the desired properties.
"""
link="https://zoom.us/j/96976751658?pwd=TjN3VmhRWjRuWVNLWHJha0dITVZtZz09"
password="5SBwfj"

+++
